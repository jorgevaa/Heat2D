import numpy as np
import matplotlib.pyplot as plt



nodes = 200
time = 3 #sekunder
a = 100
length = 50 #mm
dx = length / nodes
dy = length / nodes
dt = min(dx**2 / (4 * a), dy**2 / (4 * a))


t_noder = int(time/dt)
#Starer ved 40 grader
u = np.zeros((nodes, nodes)) + 40 



for i in range(nodes):
    u[0, i] = 40 + 40 * np.sin(i/12)
    u[-1, i] = 40 + 40 * np.sin(i/12)
    u[i, 0] = 40 + 40 * np.sin(i/12)
    u[i, -1] = 40 + 40 * np.sin(i/12)

j = round(nodes/3)
k = round((nodes/3) * 2)

while j < k:
    u[j, -1] = 100
    j += 1

fig, axis = plt.subplots()

pcm = axis.pcolormesh(u, cmap=plt.cm.jet, vmin=0, vmax=100)
plt.colorbar(pcm, ax=axis)

reps = 0

while reps < time :

    w = u.copy()

    for i in range(1, nodes - 1):
        for j in range(1, nodes - 1):

            dd_ux = (w[i-1, j] - 2*w[i, j] + w[i+1, j])/dx**2
            dd_uy = (w[i, j-1] - 2*w[i, j] + w[i, j+1])/dy**2

            u[i, j] = dt * a * (dd_ux + dd_uy) + w[i, j]

    reps += dt

    pcm.set_array(u)
    axis.set_title("Temperatur på plata ved t: {:.3f} [s].".format(reps))
    plt.pause(0.02)

plt.show()